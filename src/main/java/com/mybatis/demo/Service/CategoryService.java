package com.mybatis.demo.Service;

import com.mybatis.demo.model.Category;
import com.mybatis.demo.repository.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    private CategoryRepo categoryRepo;

    @Autowired
    public void setCategoryRepo(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    public Category findById(Integer id) {
        return categoryRepo.findById(id);
    }

    public List<Category> findAll() {
        return categoryRepo.findAll();
    }

    public List<Category> findAll(int page, int itemLimit) {
        page = (page * itemLimit) - itemLimit;
        return categoryRepo.findFilter(page, itemLimit);
    }

    public boolean insert(Category category) {
        return categoryRepo.insert(category);
    }

    public void delete(int id) {
        categoryRepo.delete(id);
    }

    public void update(Category category) {
        categoryRepo.update(category);
    }

    public int getTotalItem() {
        return categoryRepo.getTotalItem();
    }

    public int getTotalPage(int itemLimit) {
        double limit = itemLimit;
        double itemCount = getTotalItem();
        return (int)Math.ceil(itemCount / limit);
    }
}
