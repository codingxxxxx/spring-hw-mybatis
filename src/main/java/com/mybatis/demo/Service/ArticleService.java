package com.mybatis.demo.Service;

import com.mybatis.demo.model.Article;
import com.mybatis.demo.repository.ArticleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class ArticleService {


    private ArticleRepo articleRepo;

    @Autowired
    public void setArticleRepo(ArticleRepo articleRepo) {
        this.articleRepo = articleRepo;
    }

    public List<Article> findAll(Integer page, Integer limit, String title) {
        page = (page * limit) - limit;
        return articleRepo.findAll(page, limit, title.toLowerCase());
    }

    public boolean insert(Article article) {
        article.setCreateDate(LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)));
        return articleRepo.insert(article);
    }

    public List<Article> filter(Integer id, Integer limit, Integer page, String search) {
        page = (page * limit) - limit;
        return articleRepo.findByCategory(id, limit, page, search.toLowerCase());
    }

    public boolean delete(Integer id) {
        return articleRepo.delete(id);
    }

    public Article findById(Integer id){ return articleRepo.findById(id); }

    public boolean update(Article article) {
        return articleRepo.update(article);
    }

    public int getItemCount(String title) {
        return articleRepo.getItemCount(title.toLowerCase());
    }

    public int getItemCount(int categoryId, String title) {
        return articleRepo.getArticleCount(categoryId, title.toLowerCase());
    }

    public int getPageCount(int itemLimit, String title) {
        double limit = itemLimit;
        double itemCount = getItemCount(title);
        return (int)Math.ceil(itemCount / limit);
    }

    public int getPageCount(int itemLimit, int categoryId, String title) {
        double limit = itemLimit;
        double itemCount = getItemCount(categoryId, title);
        return (int)Math.ceil(itemCount / limit);
    }

}
