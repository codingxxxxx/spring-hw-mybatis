package com.mybatis.demo.repository;

import com.mybatis.demo.model.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepo {

    // select with filter
    @Select("SELECT * FROM tbl_category WHERE id = #{id}")
    @Results({
            @Result(property = "categoryName", column = "category_name")
    })
    Category findById(Integer id);

    // select all
    @Select("SELECT * FROM tbl_category")
    @Results({
            @Result(property = "categoryName", column = "category_name")
    })
    List<Category> findAll();

    // insert
    @Insert("INSERT INTO tbl_category (category_name) VALUES (#{categoryName})")
    boolean insert(Category category);

    //delete
    @Delete("DELETE FROM tbl_category WHERE id = #{id}")
    void delete(int id);

    //update
    @Update("UPDATE tbl_category SET category_name = #{categoryName} WHERE id = #{id}")
    void update(Category category);

    //select filter
    // select all
    @Select("SELECT * FROM tbl_category LIMIT #{itemLimit} OFFSET #{page}")
    @Results({
            @Result(property = "categoryName", column = "category_name")
    })
    List<Category> findFilter(int page, int itemLimit);

    @Select("SELECT COUNT(id) FROM tbl_category")
    int getTotalItem();

}
