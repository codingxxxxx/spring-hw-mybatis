package com.mybatis.demo.repository;

import com.mybatis.demo.model.Article;
import com.mybatis.demo.model.Category;
import com.mybatis.demo.provider.ArticleProvider;
import com.mybatis.demo.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepo {

    // select all field
    @Select("SELECT * FROM tbl_article WHERE LOWER(title) LIKE '%'||#{search}||'%' LIMIT #{limit} OFFSET #{page}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "category", column = "category_id", one = @One(select = "getCategory"))
    })
    List<Article> findAll(Integer page, Integer limit, String search);

    // select by category
    @Select("SELECT * FROM tbl_article WHERE category_id = #{id} AND LOWER(title) LIKE '%'||#{search}||'%' LIMIT #{limit} OFFSET #{page}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "category", column = "category_id", one = @One(select = "getCategory"))
    })
    List<Article> findByCategory(Integer id, Integer limit, Integer page, String search);

    @Select("SELECT * FROM tbl_category WHERE id = #{categoryId}")
    @Results({
            @Result(property = "categoryName", column = "category_name")
    })
    Category getCategory(int categoryId);

    // update
    @Update("UPDATE tbl_article SET title = #{title}, description = #{description}, author = #{author}, thumbnail = #{thumbnail}, create_date = #{createDate}, category_id = #{category.id} WHERE id = #{id}")
    boolean update(Article article);

    // insert
    @Insert("INSERT INTO tbl_article (title,description,author,thumbnail,create_date,category_id) VALUES (#{title},#{description},#{author},#{thumbnail},#{createDate},#{category.id})")
    boolean insert(Article article);

    // delete
    @Delete("DELETE FROM tbl_article WHERE id = #{id}")
    boolean delete(Integer id);

    // select by id
    @Select("SELECT * FROM tbl_article WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "category", column = "category_id", one = @One(select = "getCategory"))
    })
    public Article findById(Integer id);

    // count page for article
    @Select("SELECT COUNT(id) as count FROM tbl_article WHERE LOWER(title) LIKE '%'||#{title}||'%'")
    public int getItemCount(String title);

    // count page for article with filter
    @Select("SELECT COUNT(id) as count FROM tbl_article WHERE category_id = #{id} AND LOWER(title) LIKE '%'||#{title}||'%'")
    public int getArticleCount(int id, String title);

}
