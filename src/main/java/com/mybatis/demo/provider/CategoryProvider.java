package com.mybatis.demo.provider;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String findById(Integer id) {
        return new SQL(){{
            SELECT("*");
            FROM("tbl_category");
            WHERE("id = " + id);
        }}.toString();
    }

}
