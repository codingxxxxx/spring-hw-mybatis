package com.mybatis.demo.provider;

import com.mybatis.demo.Service.CategoryService;
import com.mybatis.demo.model.Article;
import com.mybatis.demo.model.Category;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ArticleProvider {

    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public String findAll() {
        return new SQL(){{
            SELECT("*");
            FROM("tbl_category");
        }}.toString();
    }

}
