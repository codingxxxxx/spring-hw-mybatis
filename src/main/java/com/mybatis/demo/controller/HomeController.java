package com.mybatis.demo.controller;

import com.mybatis.demo.Service.ArticleService;
import com.mybatis.demo.Service.CategoryService;
import com.mybatis.demo.model.Article;
import com.mybatis.demo.model.Category;
import com.mybatis.demo.repository.CategoryRepo;
import com.mybatis.demo.utils.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Controller
public class HomeController {

    private final static int ITEM_LIMIT = 3;
    @Value("${file.image.client}")
    private String clientPath;

    private ArticleService articleService;
    private CategoryService categoryService;

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping("/")
    public String home(Model model) {
        int pageCount = articleService.getPageCount(ITEM_LIMIT, "");
        List<Article> articleList = articleService.findAll(1, ITEM_LIMIT, "");
        List<Category> categoryList = categoryService.findAll();
        Category category = new Category();
        category.setCategoryName("All Category");
        category.setId(-1);
        int totalRecord = articleService.getItemCount("");
        model.addAttribute("articles", articleList);
        model.addAttribute("categories", categoryList);
        model.addAttribute("currentCategory", category);
        model.addAttribute("page", pageCount);
        model.addAttribute("totalRecords", totalRecord);
        model.addAttribute("currentPage", 1);
        model.addAttribute("search", new Search(""));
        return "/article/home";
    }

    @RequestMapping(value = {"/category/{id}/search/", "/category/{id}/search/{search}"})
    public String filterArticle(@PathVariable("id")Integer id,@PathVariable(value = "search", required = false)Optional<String> search, Model model) {
        String searchTitle = "";
        if (search.isPresent())
            searchTitle = search.get();
        int pageNumber = id > 0 ? articleService.getPageCount(ITEM_LIMIT, id, searchTitle) : articleService.getPageCount(ITEM_LIMIT, searchTitle);
        List<Article> articleList = id > 0 ? articleService.filter(id, ITEM_LIMIT, 1, searchTitle) : articleService.findAll(1, ITEM_LIMIT, searchTitle);
        List<Category> categoryList = categoryService.findAll();
        Category category = categoryService.findById(id) == null ? new Category(-1, "All Category") : categoryService.findById(id);
        int totalRecord = id > 0 ? articleService.getItemCount(id, searchTitle) : articleService.getItemCount(searchTitle);
        model.addAttribute("currentCategory", category);
        model.addAttribute("articles", articleList);
        model.addAttribute("categories", categoryList);
        model.addAttribute("page", pageNumber);
        model.addAttribute("totalRecords", totalRecord);
        model.addAttribute("currentPage", 1);
        model.addAttribute("search", new Search(searchTitle));
        return "/article/home";
    }

    @RequestMapping("/add")
    public String add(Model model) {
        model.addAttribute("article", new Article());
        model.addAttribute("categories", categoryService.findAll());
        return "/article/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addArticle(@ModelAttribute Article article, @ModelAttribute MultipartFile file) {
        if (file != null) {
            String imgCode = UUID.randomUUID().toString();
            article.setThumbnail(imgCode);
            //upload
            try {
                Files.copy(file.getInputStream(), Paths.get(clientPath, imgCode ));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        articleService.insert(article);
        return "redirect:add";
    }

    @RequestMapping("/delete/{id}")
    public String deleteArticle(@PathVariable("id") Integer id) {
        articleService.delete(id);
        return "redirect:/";
    }

    @RequestMapping("/view/{id}")
    public String viewArticle(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("article", articleService.findById(id));
        return "/article/view";
    }

    @RequestMapping("/update/{id}")
    public String updateView(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("article", articleService.findById(id));
        model.addAttribute("categories", categoryService.findAll());
        return "/article/update";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateArticle(@ModelAttribute Article article, @ModelAttribute MultipartFile file) {
        if (!file.getOriginalFilename().equals("")) {
            String imgCode = UUID.randomUUID().toString();
            try {
                Files.copy(file.getInputStream(), Paths.get(clientPath, imgCode));
                article.setThumbnail(imgCode);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        articleService.update(article);
        return "redirect:/";
    }

    @RequestMapping(value = {"/page/{p}/category/{cat}/search/", "/page/{p}/category/{cat}/search/{search}"})
    public String gotoPage(@PathVariable("p") Integer pageNumber, @PathVariable("cat") Integer cateId,@PathVariable(value = "search", required = false)Optional<String> articleTitle, Model model) {
        List<Article> articleList;
        int pageCount;
        int totalRecord;
        Category currentCategory;
        String title = articleTitle.orElse("");
        if (cateId <= 0) {
            // show all category
            articleList = articleService.findAll(pageNumber, ITEM_LIMIT, title);
            pageCount = articleService.getPageCount(ITEM_LIMIT, title);
            currentCategory = new Category();
            currentCategory.setCategoryName("All Category");
            currentCategory.setId(-1);
            totalRecord = articleService.getItemCount(title);
        } else {
            // filter article with category
            articleList = articleService.filter(cateId ,ITEM_LIMIT, pageNumber, title);
            pageCount = articleService.getPageCount(ITEM_LIMIT, cateId, title);
            currentCategory = categoryService.findById(cateId);
            totalRecord = articleService.getItemCount(cateId, title);
        }
        List<Category> categoryList = categoryService.findAll();
        model.addAttribute("articles", articleList);
        model.addAttribute("categories", categoryList);
        model.addAttribute("currentCategory", currentCategory);
        model.addAttribute("page", pageCount);
        model.addAttribute("currentPage", pageNumber);
        model.addAttribute("totalRecords", totalRecord);
        model.addAttribute("search", new Search(title));
        return "/article/home";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchArticle(@ModelAttribute Search search, @ModelAttribute Category currentCategory) {
        return "redirect:/category/" + currentCategory.getId() + "/search/" + search.getValue();
    }

    @RequestMapping("/test")
    public String test() {
        return "fragments/default";
    }

}
