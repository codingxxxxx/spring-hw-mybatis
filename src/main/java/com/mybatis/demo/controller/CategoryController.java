package com.mybatis.demo.controller;


import com.mybatis.demo.Service.CategoryService;
import com.mybatis.demo.model.Category;
import com.mybatis.demo.utils.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
public class CategoryController {

    private static final int ITEM_LIMIT = 5;

    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping(value = {"/category", "/category/{page}"})
    public String gotoCategory(Model model, @PathVariable("page")Optional<Integer> page) {
        int currentPage = page.isPresent() ? page.get() : 1;
        int totalPage = categoryService.getTotalPage(ITEM_LIMIT);
        int totalItem = categoryService.getTotalItem();
        List<Category> categoryList = categoryService.findAll(currentPage, ITEM_LIMIT);
        PageData<Category> pageData = new PageData<>(categoryList, null, totalPage, currentPage, totalItem);
        model.addAttribute("data", pageData);
        return "category/category";
    }

    @RequestMapping("/category/add")
    public String add(Model model) {
        model.addAttribute("category", new Category());
        return "category/add";
    }

    @RequestMapping(value = "/category/add", method = RequestMethod.POST)
    public String addCategory(@ModelAttribute Category category) {
        categoryService.insert(category);
        return "redirect:/category/add";
    }

    @RequestMapping("/category/delete/{id}")
    public String deleteCategory(@PathVariable("id") int id) {
        categoryService.delete(id);
        return "redirect:/category";
    }

    @RequestMapping("/category/update/{id}")
    public String updateCategory(@PathVariable("id") int id, Model model) {
        Category category = categoryService.findById(id);
        model.addAttribute("category", category);
        return "category/update";
    }

    @RequestMapping(value = "/category/update", method = RequestMethod.POST)
    public String update(@ModelAttribute Category category) {
        categoryService.update(category);
        return "redirect:/category";
    }

}
