package com.mybatis.demo.utils;

import java.util.List;

public class PageData <T> {

    private List<T> listData;
    private T data;
    private int totalPage;
    private int currentPage;
    private int totalItem;

    public PageData(List<T> listData, T data, int totalPage, int currentPage, int totalItem) {
        this.listData = listData;
        this.data = data;
        this.totalPage = totalPage;
        this.currentPage = currentPage;
        this.totalItem = totalItem;
    }

    public List<T> getListData() {
        return listData;
    }

    public void setListData(List<T> listData) {
        this.listData = listData;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(int totalItem) {
        this.totalItem = totalItem;
    }
}
