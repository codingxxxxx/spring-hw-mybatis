CREATE TABLE tbl_category (
    id int primary key auto_increment,
    category_name varchar (50),
);

CREATE TABLE tbl_article (
    id int primary key auto_increment,
    title varchar (100) not null ,
    description varchar (200),
    author varchar (100),
    thumbnail varchar (100),
    create_date varchar (50),
    category_id int,
    FOREIGN  KEY (category_id) REFERENCES tbl_category (id) on DELETE  CASCADE

);

INSERT INTO tbl_category (category_name) VALUES ('Fruit');
INSERT INTO tbl_category (category_name) VALUES ('Animal');
INSERT INTO tbl_category (category_name) VALUES ('Vegetable');
INSERT INTO tbl_category (category_name) VALUES ('General Knowledge');
INSERT INTO tbl_category (category_name) VALUES ('Fast Food');
INSERT INTO tbl_category (category_name) VALUES ('Drink');
INSERT INTO tbl_category (category_name) VALUES ('Beer');
INSERT INTO tbl_category (category_name) VALUES ('Other');

INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Apple', 'Apple is awesome and cool', 'john', '', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Banana', 'Some random data', 'john', '', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Orange', 'Some random data', 'john', '', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Mango', 'Some random data', 'john', '', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Papaya', 'Some random data', 'john', '', '12/2/2018',3);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Tiger', 'Some random data', 'john', '', '12/2/2018',2);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Cat', 'Some random data', 'john', '', '12/2/2018',2);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Dog', 'Some random data', 'john', '', '12/2/2018',2);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Elephant', 'Some random data', 'john', '', '12/2/2018',2);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Lion', 'Some random data', 'john', '', '12/2/2018',2);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Snake', 'Some random data', 'john', '', '12/2/2018',2);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Java', 'Some random data', 'john', '', '12/2/2018',4);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('C# Programming', 'Some random data', 'john', '', '12/2/2018',4);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('PHP Programming', 'Some random data', 'john', '', '12/2/2018',4);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Ruby Programming', 'Some random data', 'john', '', '12/2/2018',4);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('JS Programming', 'Some random data', 'john', '', '12/2/2018',4);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Backend JAVA', 'Some random data', 'john', '', '12/2/2018',4);